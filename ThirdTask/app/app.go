package app

import (
	"ThirdTask/internal/config"
	"ThirdTask/internal/handlers"
	"ThirdTask/internal/repository"
	"ThirdTask/internal/service"
	"context"
	"errors"
	"log"
	"net/http"
)

type App struct {
	config        *config.Config
	server        *http.Server
	service       *service.Service
	serverService *handlers.ServerService
}

func NewApp() *App {
	return &App{}
}

func (a *App) Init(cfg *config.Config) error {
	a.config = cfg
	repo := repository.NewRepository()
	a.service = service.NewService(repo)
	a.serverService = handlers.NewServer(a.service)
	a.newServer()
	return nil
}

func (a *App) newServer() {
	mux := http.NewServeMux()
	mux.HandleFunc("/getbalance", a.serverService.GetBalance)
	mux.HandleFunc("/changebalance", a.serverService.ChangeBalance)
	mux.HandleFunc("/transaction", a.serverService.Transaction)
	a.server = &http.Server{
		Handler: mux,
		Addr:    a.config.Http.Port,
	}

}

func (a *App) Start() error {

	go func() {
		if err := a.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("Could not listen on port :%s: %s\n", a.config.Http.Port, err.Error())
		}
	}()

	log.Printf(
		"Api start at port :%s\n",
		a.config.Http.Port,
	)

	return nil
}

func (a *App) Stop(ctx context.Context) error {
	<-ctx.Done()

	done := make(chan bool)
	log.Printf("Server is shutting down...")

	go func() {
		log.Printf("Server stopped")
		close(done)
	}()

	<-done
	return nil
}
