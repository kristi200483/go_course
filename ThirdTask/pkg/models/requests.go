package models

type RequestGetBalance struct {
	Id string `json:"userId"`
}

type RequestChangeBalance struct {
	Id      string `json:"userId"`
	Balance int32  `json:"balance"`
}

type RequestTransaction struct {
	IdFrom  string `json:"userIdFrom"`
	IdTo    string `json:"userIdTo"`
	Balance int32  `json:"balance"`
}
