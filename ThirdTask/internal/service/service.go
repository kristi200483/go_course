package service

import (
	"ThirdTask/pkg/models"
)

type Service struct {
	Repository UserRepository
}

func NewService(Repository UserRepository) *Service {
	return &Service{
		Repository: Repository,
	}
}

func (s *Service) GetBalance(req models.RequestGetBalance) (int32, string) {
	user, err := s.Repository.GetUser(req.Id)
	if err != "" {
		return 0, err
	}
	return user.Balance, ""
}

func (s *Service) ChangeBalance(req models.RequestChangeBalance) (int32, string) {
	user, err := s.Repository.GetUser(req.Id)
	if err != "" {
		return 0, err
	}
	if user.Balance+req.Balance < 0 {
		return 0, "Negative balance"
	}
	user, err = s.Repository.SetBalance(req.Id, req.Balance)
	return user.Balance, ""
}

func (s *Service) Transaction(req models.RequestTransaction) string {
	userfrom, err := s.Repository.GetUser(req.IdFrom)
	if err != "" {
		return err
	}
	if userfrom.Balance < req.Balance {
		return "No enough money"
	}
	userto, err := s.Repository.GetUser(req.IdTo)
	if err != "" {
		return err
	}
	if userfrom == userto {
		return "Can't make transfer to this user"
	}
	if userto.Balance+req.Balance < 0 {
		return "Incorrect summa"
	}
	s.Repository.SetBalance(req.IdFrom, userfrom.Balance-req.Balance)
	s.Repository.SetBalance(req.IdTo, userto.Balance+req.Balance)
	return "OK"
}
