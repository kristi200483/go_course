package service

import (
	"ThirdTask/pkg/models"
)

type UserRepository interface {
	GetUser(id string) (*models.User, string)
	SetBalance(id string, balance int32) (*models.User, string)
}
