package config

import (
	"gopkg.in/yaml.v3"
	"os"
)

type Config struct {
	Http Http `yaml:http`
}

type Http struct {
	Port string `yaml:port`
}

func Parse(filePath string) (*Config, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	var config Config
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}
