package repository

import (
	"ThirdTask/pkg/models"
)

type Repository struct {
	Storage map[string]int32
}

func NewRepository() *Repository {
	return &Repository{
		Storage: map[string]int32{"1": 0, "2": 100},
	}
}

func CreateUserDTO(balance int32, id string) *models.User {
	return &models.User{
		Id:      id,
		Balance: balance,
	}
}

func (r Repository) GetUser(id string) (*models.User, string) {
	balance, err := r.Storage[id]
	if err == false {
		return nil, "User not found"
	}
	return CreateUserDTO(balance, id), ""
}

func (r Repository) SetBalance(id string, balance int32) (*models.User, string) {
	summa, err := r.Storage[id]
	if err == false {
		return nil, "User not found"
	}
	r.Storage[id] = balance + summa
	return CreateUserDTO(balance, id), ""
}
