package handlers

import (
	"ThirdTask/internal/service"
	"ThirdTask/pkg/models"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
)

type ServerService struct {
	TransactionService *service.Service
}

type Response struct {
	Answer string `json:"answer"`
	Status string `json:"status"`
	Error  string `json:"error"`
}

func NewServer(TransactionalService *service.Service) *ServerService {
	return &ServerService{
		TransactionService: TransactionalService,
	}
}

func (s *ServerService) GetBalance(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := io.ReadAll(r.Body)
	var req models.RequestGetBalance
	err := json.Unmarshal(reqBody, &req)
	if err != nil {
		req.Id = "0"
	}
	answer, er := s.TransactionService.GetBalance(req)
	ans, _ := json.Marshal(Response{
		Answer: strconv.Itoa(int(answer)),
		Status: "OK",
		Error:  er,
	})
	if er != "" {
		ans, _ = json.Marshal(Response{
			Answer: "",
			Status: "Error",
			Error:  er,
		})
	}
	_, _ = w.Write(ans)
	return
}

func (s *ServerService) ChangeBalance(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := io.ReadAll(r.Body)
	var req models.RequestChangeBalance
	err := json.Unmarshal(reqBody, &req)
	if err != nil {
		req.Balance = 0
		req.Id = "0"
	}
	answer, er := s.TransactionService.ChangeBalance(req)
	ans, _ := json.Marshal(Response{
		Answer: strconv.Itoa(int(answer)),
		Status: "OK",
		Error:  er,
	})
	if er != "" {
		ans, _ = json.Marshal(Response{
			Answer: "",
			Status: "Error",
			Error:  er,
		})
	}
	_, _ = w.Write(ans)
	return
}

func (s *ServerService) Transaction(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := io.ReadAll(r.Body)
	var req models.RequestTransaction
	err := json.Unmarshal(reqBody, &req)
	if err != nil {
		req.Balance = 0
		req.IdTo = "0"
		req.IdFrom = "0"
	}
	result := s.TransactionService.Transaction(req)
	ans, _ := json.Marshal(Response{
		Answer: result,
		Status: "OK",
		Error:  "",
	})
	if result != "OK" {
		ans, _ = json.Marshal(Response{
			Answer: "",
			Status: "Error",
			Error:  result,
		})
	}
	_, _ = w.Write(ans)
	return
}
