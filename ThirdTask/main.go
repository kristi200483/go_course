package main

import (
	"ThirdTask/app"
	"ThirdTask/internal/config"
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	var filepath string
	flag.StringVar(&filepath, "c", "config.yaml", "set config path")
	flag.Parse()

	cfg, err := config.Parse(filepath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	a := app.NewApp()
	err = a.Init(cfg)
	if err != nil {
		fmt.Println(err)
	}
	go func() {
		err := a.Start()
		if err != nil {
			fmt.Println(err)
		}
	}()
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()
	defer func(a *app.App, ctx context.Context) {
		err := a.Stop(ctx)
		if err != nil {
			fmt.Println(err)
		}
	}(a, ctx)

	<-ctx.Done()
}
