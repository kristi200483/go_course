package app

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"time"
)

type Client struct {
	client *http.Client
}

type PostRequest struct {
	InputString string `json:"inputString"`
}

func NewClient() *Client {
	return &Client{
		client: &http.Client{Timeout: time.Duration(15) * time.Second},
	}
}

func (c *Client) CheckVersion() {
	resp, err := c.client.Get("http://localhost:8080/version")
	if err != nil {
		fmt.Printf("Error %s\n", err)
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("error while closing response body")
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error while reading a response")
		return
	} else {
		fmt.Printf("%s\n", body)
	}
	return
}

func (c *Client) Decode(input string) {
	fmt.Println(input)
	req := PostRequest{input}
	jsonReq, err := json.Marshal(req)
	if err != nil {
		return
	}
	resp, err := c.client.Post("http://localhost:8080/decode", "application/json", bytes.NewBuffer(jsonReq))
	if err != nil {
		fmt.Println(err)
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("error while closing response body")
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error while reading a response")
		return
	} else {
		fmt.Printf("%s\n", body)
	}
	return
}

func (c *Client) HardOp() {
	resp, err := c.client.Get("http://localhost:8080/hard-op")
	if resp == nil {
		fmt.Println("Time is over, request failed")
	} else {
		fmt.Println(resp.StatusCode)
	}
	if err != nil {
		fmt.Printf("Error %s\n", err)
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("error while closing response body")
			return
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error while reading a response")
		return
	} else {
		fmt.Printf("%s\n", body)
	}
	return
}

func CreateString(size int) string {
	symbols := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
	answer := make([]rune, size)
	for i := 0; i < size; i++ {
		answer[i] = symbols[rand.Intn(len(symbols))]
	}
	return base64.StdEncoding.EncodeToString([]byte(string(answer)))
}
