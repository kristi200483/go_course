package main

import (
	"Client/app"
)

func main() {
	client := app.NewClient()
	client.CheckVersion()
	client.HardOp()
	inputString := app.CreateString(10)
	client.Decode(inputString)
}
