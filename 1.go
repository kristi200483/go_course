package main

import (
	"fmt"
	"hash/fnv"
)

type Book struct {
	title  string
	author string
}

func NewBook() *Book {
	return &Book{
		title:  "",
		author: "",
	}
}

func (b Book) GetTitle() string {
	return b.title
}

func (b Book) GetAuthor() string {
	return b.author
}

func (StorageOnMap) Hash(title string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(title))
	return h.Sum32()
}

type StorageOnMap struct {
	storage map[uint32]Book
}

func NewStorageOnMap() *StorageOnMap {
	return &StorageOnMap{
		storage: make(map[uint32]Book),
	}
}

func (s *StorageOnMap) AddBook(book Book) {
	hash := s.Hash(book.title)
	s.storage[hash] = book
	fmt.Println("Добавлена книга", book.title, book.author)
}

func (s StorageOnMap) FindBook(hash uint32) (*Book, bool) {
	data, ok := s.storage[hash]
	if ok {
		return &data, ok
	} else {
		return NewBook(), ok
	}
}

func (StorageOnSlice) Hash(title string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(title))
	return h.Sum32() % (1e9 + 7)
}

type StorageOnSlice struct {
	storage []Book
	index   map[uint32]int
	counter int
}

func NewStorageOnSlice() *StorageOnSlice {
	return &StorageOnSlice{
		storage: make([]Book, 1),
		index:   make(map[uint32]int),
		counter: 1,
	}
}

func (s *StorageOnSlice) AddBook(book Book) {
	hash := s.Hash(book.title)
	s.index[hash] = s.counter
	s.storage = append(s.storage, book)
	s.counter++
	fmt.Println("Добавлена книга", book.title, book.author)
}

func (s StorageOnSlice) FindBook(hash uint32) (*Book, bool) {
	index, ok := s.index[hash]
	if ok {
		return &s.storage[index], ok
	} else {
		return NewBook(), ok
	}
}

type Storage interface {
	AddBook(Book)
	FindBook(uint32) (*Book, bool)
	Hash(string) uint32
}

type Library struct {
	Storage
}

func NewLibrary(storage Storage) *Library {
	return &Library{
		storage,
	}
}

func (l Library) AddNewBook(book Book) {
	l.Storage.AddBook(book)
}

func (l Library) FindBook(title string) {
	book, ok := l.Storage.FindBook(l.Storage.Hash(title))
	if ok {
		fmt.Println("Книга найдена: ")
		fmt.Println("Название", book.title, ", Автор", book.author)
	} else {
		fmt.Println("Книга не найдена")
	}
}

func main() {
	//Books for library
	first := Book{"First", "Arthur"}
	second := Book{"Second", "Igor"}
	third := Book{"Third", "Alexey"}
	forth := Book{"Forth", "Ivan"}
	fifth := Book{"Fifth", "Maria"}
	//Library with map storage
	fmt.Println("Library with map storage-----")
	storage := NewStorageOnMap()
	lib := NewLibrary(storage)
	lib.AddNewBook(first)
	lib.AddNewBook(second)
	lib.AddNewBook(third)
	lib.AddNewBook(forth)
	lib.AddNewBook(fifth)
	//try to find book not from library
	lib.FindBook("Back")
	//find book in library
	lib.FindBook("Third")
	lib.FindBook("Forth")
	//library with slice storage
	fmt.Println("Library with slice storage-----")
	storage2 := NewStorageOnSlice()
	lib2 := NewLibrary(storage2)
	lib2.AddNewBook(first)
	lib2.AddNewBook(second)
	lib2.AddNewBook(third)
	lib2.AddNewBook(forth)
	lib2.AddNewBook(fifth)
	//try to find book not from library
	lib2.FindBook("Front")
	//find book in library
	lib2.FindBook("Third")
	lib2.FindBook("Forth")
}
