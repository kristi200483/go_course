package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"Server/app"
	"Server/internal/config"
)

func main() {
	var filepath string
	flag.StringVar(&filepath, "c", ".config.yaml", "set config path")
	flag.Parse()

	cfg, err := config.Parse(filepath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	a := app.NewApp(cfg)
	go a.Run()
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()
	defer a.Stop(ctx)

	<-ctx.Done()
}
