package services

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"log"
	"math/rand"
	"net/http"
	"time"
)

type Request struct {
	StringForDecode string `json:"inputString"`
}

type Response struct {
	AnswerString string `json:"answerString"`
	StatusDecode string `json:"statusDecode"`
	Error        error  `json:"error"`
}

func ReturnApiVersion(w http.ResponseWriter, _ *http.Request) {
	_, err := w.Write([]byte("v0.0.1"))
	if err != nil {
		return
	}
}

func HardOp(w http.ResponseWriter, _ *http.Request) {
	time.Sleep(time.Duration(rand.Intn(10)+10) * time.Second)
	result := rand.Intn(2)
	if result == 0 {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func Decode(w http.ResponseWriter, r *http.Request) {
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal("failed to close request body")
		}
	}(r.Body)

	reqBody, err := io.ReadAll(r.Body)
	if err != nil {
		log.Fatal("failed to read request body")
	}

	var req Request
	err = json.Unmarshal(reqBody, &req)
	if err != nil {
		log.Fatal("failed to unmarshal request")
	}

	decodedString, err := base64.StdEncoding.DecodeString(req.StringForDecode)
	if err != nil {
		log.Fatal("failed to post inputString")
	}

	ans, err := json.Marshal(Response{
		AnswerString: string(decodedString),
		StatusDecode: "OK",
		Error:        err,
	})
	if err != nil {
		log.Fatal("failed to Marshal json")
	}

	_, err = w.Write(ans)
	if err != nil {
		log.Fatal("failed write")
	}
	return
}
