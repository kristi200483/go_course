package app

import (
	"Server/internal/config"
	"Server/internal/services"
	"context"
	"log"
	"net/http"
)

type App struct {
	config *config.Config
	server *http.Server
}

func NewApp(config *config.Config) *App {
	return &App{
		config: config,
		server: &http.Server{
			Addr:    config.Http.Port,
			Handler: InitServer(config),
		},
	}
}

func (a *App) Run() {
	go func() {
		err := a.server.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

}

func (a *App) Stop(context context.Context) {
	log.Fatal(a.server.Shutdown(context))

}

func InitServer(cfg *config.Config) http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/version", services.ReturnApiVersion)
	mux.HandleFunc("/decode", services.Decode)
	mux.HandleFunc("/hard-op", services.HardOp)

	return mux
}
